let previousScreenBuffer = "0";
let currentScreenBuffer = "0";
let previousOperator = "";

const screen = document.querySelector(".screen table tr td");

function display(text) {
  screen.innerText = text;
}

function reset() {
  previousScreenBuffer = "0";
  currentScreenBuffer = "0";
  previousOperator = "";
}

function isCharDigit(c) {
  return c.length === 1 && c >= "0" && c <= "9";
}

function isOperator(c) {
  return c === "+" || c === "−" || c === "×" || c === "÷";
}

function addCharToCurrent(buttonValue) {
  if (!isFinite(currentScreenBuffer)) {
    reset();
  }
  if (currentScreenBuffer === "0") {
    currentScreenBuffer = buttonValue;
  } else if (currentScreenBuffer.length <= 20) {
    currentScreenBuffer += buttonValue;
  }
}

function removeCharFromCurrent() {
  if (currentScreenBuffer.length === 1) {
    currentScreenBuffer = "0";
  } else {
    currentScreenBuffer = currentScreenBuffer.slice(
      0,
      currentScreenBuffer.length - 1
    );
  }
}

function precise(x) {
  return Number.parseFloat(x).toPrecision();
}

function calculateResult() {
  if ({ previousOperator } === "") {
    return;
  }
  const num1 = parseFloat(previousScreenBuffer);
  const num2 = parseFloat(currentScreenBuffer);
  let result = 0;
  if (previousOperator === "+") {
    result = num1 + num2;
  } else if (previousOperator === "−") {
    result = num1 - num2;
  } else if (previousOperator === "×") {
    result = num1 * num2;
  } else if (previousOperator === "÷") {
    result = num1 / num2;
  }
  reset();
  currentScreenBuffer = precise(result);
}

function setOperator(c) {
  if (previousOperator !== "") {
    calculateResult();
  }
  previousScreenBuffer = currentScreenBuffer;
  currentScreenBuffer = "0";
  previousOperator = c;
}

function init() {
  document
    .querySelector(".calc-buttons")
    .addEventListener("click", function (event) {
      if (event.currentTarget.tagName !== "section") {
        buttonValue = event.target.innerText;
        if (buttonValue === "C") {
          reset();
        } else if (isCharDigit(buttonValue)) {
          addCharToCurrent(buttonValue);
        } else if (buttonValue === "←") {
          removeCharFromCurrent();
        } else if (buttonValue === "=" && previousOperator.length) {
          calculateResult();
        } else if (isOperator(buttonValue)) {
          setOperator(buttonValue);
        }
        display(currentScreenBuffer);
      }
    });
}

init();
